/**
 * @name dictionary
 * @const
 * @type {Object}
 * @desc Словарь
 *
 * @example
 * // i18n('my_example_value'); //Пример
 * my_example_value: {
 *      ru: 'Пример',
 *      en: 'Example'
 * }
 *
 * @example <caption>данные</caption>
 * // i18n('my_example_value', ['example']); //Пример с данными example
 * my_example_value: {
 *      ru: 'Пример с данными {0}',
 *      en: 'Example with data {0}'
 * }
 *
 * @example <caption>склонение</caption>
 * // i18n('my_example_value', [12]); //Пример с 12 данных
 * my_example_value: {
 *      ru: 'Пример с {0} [данным, данными, данных]',
 *      en: 'Example with {0} data'
 * }
 */
export default {
    copy_notification: {
        ru: 'Текст скопирован в буфер обмена',
        en: 'Text was copy in clipboard'
    }
};

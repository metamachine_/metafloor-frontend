import store from '@/store';

/**
 * @module middlewares
 * @desc Модуль мидлвейров для роутеров
 * <br>
 * Каждый мидлвейр должен возвращать <b>Promise</b>
 *
 * @example
 * {
 *      name: 'my-route',
 *      path: '/my-route',
 *      meta: {
 *          middleware: [myMiddleware]
 *      }
 * }
 */

/**
  @function
  @desc Основной мидлвейр. Запускает каждый раз перед переходом
  */
export const mainBefore = (to, from, next) => {
    store.dispatch('core/pageLoading/show');

    if (store.state.app.initialized) {
        next();
    } else {
        store
            .dispatch('app/init')
            .then(next)
            .catch(() => {
                next({
                    name: 'error',
                    params: {
                        code: 500
                    }
                });
            });
    }
};

/**
  @function
  @desc Основной мидлвейр. Запускает каждый раз после перехода
  */
export const mainAfter = (to, from) => {
    if (!store.state.app.initialized) {
        store.dispatch('app/setInitialized');
    }

    if (!to.meta.saveScrollPosition) {
        setTimeout(() => {
            window.scrollTo(0, 0);
        });
    }

    store.dispatch('core/pageLoading/hide');
};

export default [
    {
        data: {
            status: 'open',
            course: '10345.32 USD',
            confirmations: 1
        },
        info: [
            {
                id: 'id',
                value: '1406-I82193'
            },
            {
                id: 'date',
                value: '14.06.2019 18:12:13'
            },
            {
                id: 'tax',
                value: 'Тариф: 5%'
            },
            {
                id: 'status',
                value: 'Открыто'
            }
        ],
        amount: {
            value: 10433.35,
            currency: 'RUR',
            extra: ', к зачислению 96600000.00 RUR'
        },
        req: {
            value: 'Bitcoin, 1NeJEFzY8PbVS9RvYPfDP93iqXxHjav791',
            info: 'Lorem ipsum dolor sit amet consectetur adipisicing elit.'
        },
        actions: [
            {
                action: 'error',
                label: 'Сообщить об ошибке'
            }
        ]
    },
    {
        data: {
            status: 'open',
            course: '100345.32 USD',
            confirmations: 2
        },
        info: [
            {
                id: 'id',
                value: '1406-I82193'
            },
            {
                id: 'date',
                value: '14.06.2019 18:12:13'
            },
            {
                id: 'tax',
                value: 'Тариф 1%'
            },
            {
                id: 'status',
                value: 'Открыто'
            }
        ],
        amount: {
            value: 1015438.95,
            currency: 'USD',
            extra: ', к зачислению 96600000.00 RUR'
        },
        req: {
            value: 'DASH, 1NeJEFzY8PbVS9RvYPfDP93iqXxHjav791',
            info: 'Lorem ipsum dolor sit amet consectetur adipisicing elit.'
        },
        actions: [
            {
                action: 'error',
                label: 'Сообщить об ошибке'
            }
        ]
    },
    {
        data: {
            status: 'open',
            course: '10345.32 USD',
            confirmations: 1
        },
        info: [
            {
                id: 'id',
                value: '1406-I82193'
            },
            {
                id: 'date',
                value: '14.06.2019 18:12:13'
            },
            {
                id: 'tax',
                value: 'Тариф: 5%'
            },
            {
                id: 'status',
                value: 'Открыто'
            }
        ],
        amount: {
            value: 10433.35,
            currency: 'RUR',
            extra: ', к зачислению 96600000.00 RUR'
        },
        req: {
            value: 'Bitcoin, 1NeJEFzY8PbVS9RvYPfDP93iqXxHjav791',
            info: 'Lorem ipsum dolor sit amet consectetur adipisicing elit.'
        },
        actions: [
            {
                action: 'error',
                label: 'Сообщить об ошибке'
            }
        ]
    },
    {
        data: {
            status: 'open',
            course: '100345.32 USD',
            confirmations: 2
        },
        info: [
            {
                id: 'id',
                value: '1406-I82193'
            },
            {
                id: 'date',
                value: '14.06.2019 18:12:13'
            },
            {
                id: 'tax',
                value: 'Тариф 1%'
            },
            {
                id: 'status',
                value: 'Открыто'
            }
        ],
        amount: {
            value: 1015438.95,
            currency: 'USD',
            extra: ', к зачислению 96600000.00 RUR'
        },
        req: {
            value: 'DASH, 1NeJEFzY8PbVS9RvYPfDP93iqXxHjav791',
            info: 'Lorem ipsum dolor sit amet consectetur adipisicing elit.'
        },
        actions: [
            {
                action: 'error',
                label: 'Сообщить об ошибке'
            }
        ]
    },
    {
        data: {
            status: 'open',
            course: '10345.32 USD',
            confirmations: 1
        },
        info: [
            {
                id: 'id',
                value: '1406-I82193'
            },
            {
                id: 'date',
                value: '14.06.2019 18:12:13'
            },
            {
                id: 'tax',
                value: 'Тариф: 5%'
            },
            {
                id: 'status',
                value: 'Открыто'
            }
        ],
        amount: {
            value: 10433.35,
            currency: 'RUR',
            extra: ', к зачислению 96600000.00 RUR'
        },
        req: {
            value: 'Bitcoin, 1NeJEFzY8PbVS9RvYPfDP93iqXxHjav791',
            info: 'Lorem ipsum dolor sit amet consectetur adipisicing elit.'
        },
        actions: [
            {
                action: 'error',
                label: 'Сообщить об ошибке'
            }
        ]
    },
    {
        data: {
            status: 'open',
            course: '100345.32 USD',
            confirmations: 2
        },
        info: [
            {
                id: 'id',
                value: '1406-I82193'
            },
            {
                id: 'date',
                value: '14.06.2019 18:12:13'
            },
            {
                id: 'tax',
                value: 'Тариф 1%'
            },
            {
                id: 'status',
                value: 'Открыто'
            }
        ],
        amount: {
            value: 1015438.95,
            currency: 'USD',
            extra: ', к зачислению 96600000.00 RUR'
        },
        req: {
            value: 'DASH, 1NeJEFzY8PbVS9RvYPfDP93iqXxHjav791',
            info: 'Lorem ipsum dolor sit amet consectetur adipisicing elit.'
        },
        actions: [
            {
                action: 'error',
                label: 'Сообщить об ошибке'
            }
        ]
    },
    {
        data: {
            status: 'open',
            course: '10345.32 USD',
            confirmations: 1
        },
        info: [
            {
                id: 'id',
                value: '1406-I82193'
            },
            {
                id: 'date',
                value: '14.06.2019 18:12:13'
            },
            {
                id: 'tax',
                value: 'Тариф: 5%'
            },
            {
                id: 'status',
                value: 'Открыто'
            }
        ],
        amount: {
            value: 10433.35,
            currency: 'RUR',
            extra: ', к зачислению 96600000.00 RUR'
        },
        req: {
            value: 'Bitcoin, 1NeJEFzY8PbVS9RvYPfDP93iqXxHjav791',
            info: 'Lorem ipsum dolor sit amet consectetur adipisicing elit.'
        },
        actions: [
            {
                action: 'error',
                label: 'Сообщить об ошибке'
            }
        ]
    },
    {
        data: {
            status: 'open',
            course: '100345.32 USD',
            confirmations: 2
        },
        info: [
            {
                id: 'id',
                value: '1406-I82193'
            },
            {
                id: 'date',
                value: '14.06.2019 18:12:13'
            },
            {
                id: 'tax',
                value: 'Тариф 1%'
            },
            {
                id: 'status',
                value: 'Открыто'
            }
        ],
        amount: {
            value: 1015438.95,
            currency: 'USD',
            extra: ', к зачислению 96600000.00 RUR'
        },
        req: {
            value: 'DASH, 1NeJEFzY8PbVS9RvYPfDP93iqXxHjav791',
            info: 'Lorem ipsum dolor sit amet consectetur adipisicing elit.'
        },
        actions: [
            {
                action: 'error',
                label: 'Сообщить об ошибке'
            }
        ]
    },
    {
        data: {
            status: 'open',
            course: '10345.32 USD',
            confirmations: 1
        },
        info: [
            {
                id: 'id',
                value: '1406-I82193'
            },
            {
                id: 'date',
                value: '14.06.2019 18:12:13'
            },
            {
                id: 'tax',
                value: 'Тариф: 5%'
            },
            {
                id: 'status',
                value: 'Открыто'
            }
        ],
        amount: {
            value: 10433.35,
            currency: 'RUR',
            extra: ', к зачислению 96600000.00 RUR'
        },
        req: {
            value: 'Bitcoin, 1NeJEFzY8PbVS9RvYPfDP93iqXxHjav791',
            info: 'Lorem ipsum dolor sit amet consectetur adipisicing elit.'
        },
        actions: [
            {
                action: 'error',
                label: 'Сообщить об ошибке'
            }
        ]
    },
    {
        data: {
            status: 'open',
            course: '100345.32 USD',
            confirmations: 2
        },
        info: [
            {
                id: 'id',
                value: '1406-I82193'
            },
            {
                id: 'date',
                value: '14.06.2019 18:12:13'
            },
            {
                id: 'tax',
                value: 'Тариф 1%'
            },
            {
                id: 'status',
                value: 'Открыто'
            }
        ],
        amount: {
            value: 1015438.95,
            currency: 'USD',
            extra: ', к зачислению 96600000.00 RUR'
        },
        req: {
            value: 'DASH, 1NeJEFzY8PbVS9RvYPfDP93iqXxHjav791',
            info: 'Lorem ipsum dolor sit amet consectetur adipisicing elit.'
        },
        actions: [
            {
                action: 'error',
                label: 'Сообщить об ошибке'
            }
        ]
    },
    {
        data: {
            status: 'open',
            course: '10345.32 USD',
            confirmations: 1
        },
        info: [
            {
                id: 'id',
                value: '1406-I82193'
            },
            {
                id: 'date',
                value: '14.06.2019 18:12:13'
            },
            {
                id: 'tax',
                value: 'Тариф: 5%'
            },
            {
                id: 'status',
                value: 'Открыто'
            }
        ],
        amount: {
            value: 10433.35,
            currency: 'RUR',
            extra: ', к зачислению 96600000.00 RUR'
        },
        req: {
            value: 'Bitcoin, 1NeJEFzY8PbVS9RvYPfDP93iqXxHjav791',
            info: 'Lorem ipsum dolor sit amet consectetur adipisicing elit.'
        },
        actions: [
            {
                action: 'error',
                label: 'Сообщить об ошибке'
            }
        ]
    },
    {
        data: {
            status: 'open',
            course: '100345.32 USD',
            confirmations: 2
        },
        info: [
            {
                id: 'id',
                value: '1406-I82193'
            },
            {
                id: 'date',
                value: '14.06.2019 18:12:13'
            },
            {
                id: 'tax',
                value: 'Тариф 1%'
            },
            {
                id: 'status',
                value: 'Открыто'
            }
        ],
        amount: {
            value: 1015438.95,
            currency: 'USD',
            extra: ', к зачислению 96600000.00 RUR'
        },
        req: {
            value: 'DASH, 1NeJEFzY8PbVS9RvYPfDP93iqXxHjav791',
            info: 'Lorem ipsum dolor sit amet consectetur adipisicing elit.'
        },
        actions: [
            {
                action: 'error',
                label: 'Сообщить об ошибке'
            }
        ]
    },
    {
        data: {
            status: 'open',
            course: '10345.32 USD',
            confirmations: 1
        },
        info: [
            {
                id: 'id',
                value: '1406-I82193'
            },
            {
                id: 'date',
                value: '14.06.2019 18:12:13'
            },
            {
                id: 'tax',
                value: 'Тариф: 5%'
            },
            {
                id: 'status',
                value: 'Открыто'
            }
        ],
        amount: {
            value: 10433.35,
            currency: 'RUR',
            extra: ', к зачислению 96600000.00 RUR'
        },
        req: {
            value: 'Bitcoin, 1NeJEFzY8PbVS9RvYPfDP93iqXxHjav791',
            info: 'Lorem ipsum dolor sit amet consectetur adipisicing elit.'
        },
        actions: [
            {
                action: 'error',
                label: 'Сообщить об ошибке'
            }
        ]
    },
    {
        data: {
            status: 'open',
            course: '100345.32 USD',
            confirmations: 2
        },
        info: [
            {
                id: 'id',
                value: '1406-I82193'
            },
            {
                id: 'date',
                value: '14.06.2019 18:12:13'
            },
            {
                id: 'tax',
                value: 'Тариф 1%'
            },
            {
                id: 'status',
                value: 'Открыто'
            }
        ],
        amount: {
            value: 1015438.95,
            currency: 'USD',
            extra: ', к зачислению 96600000.00 RUR'
        },
        req: {
            value: 'DASH, 1NeJEFzY8PbVS9RvYPfDP93iqXxHjav791',
            info: 'Lorem ipsum dolor sit amet consectetur adipisicing elit.'
        },
        actions: [
            {
                action: 'error',
                label: 'Сообщить об ошибке'
            }
        ]
    },
    {
        data: {
            status: 'open',
            course: '10345.32 USD',
            confirmations: 1
        },
        info: [
            {
                id: 'id',
                value: '1406-I82193'
            },
            {
                id: 'date',
                value: '14.06.2019 18:12:13'
            },
            {
                id: 'tax',
                value: 'Тариф: 5%'
            },
            {
                id: 'status',
                value: 'Открыто'
            }
        ],
        amount: {
            value: 10433.35,
            currency: 'RUR',
            extra: ', к зачислению 96600000.00 RUR'
        },
        req: {
            value: 'Bitcoin, 1NeJEFzY8PbVS9RvYPfDP93iqXxHjav791',
            info: 'Lorem ipsum dolor sit amet consectetur adipisicing elit.'
        },
        actions: [
            {
                action: 'error',
                label: 'Сообщить об ошибке'
            }
        ]
    },
    {
        data: {
            status: 'open',
            course: '100345.32 USD',
            confirmations: 2
        },
        info: [
            {
                id: 'id',
                value: '1406-I82193'
            },
            {
                id: 'date',
                value: '14.06.2019 18:12:13'
            },
            {
                id: 'tax',
                value: 'Тариф 1%'
            },
            {
                id: 'status',
                value: 'Открыто'
            }
        ],
        amount: {
            value: 1015438.95,
            currency: 'USD',
            extra: ', к зачислению 96600000.00 RUR'
        },
        req: {
            value: 'DASH, 1NeJEFzY8PbVS9RvYPfDP93iqXxHjav791',
            info: 'Lorem ipsum dolor sit amet consectetur adipisicing elit.'
        },
        actions: [
            {
                action: 'error',
                label: 'Сообщить об ошибке'
            }
        ]
    },
    {
        data: {
            status: 'open',
            course: '10345.32 USD',
            confirmations: 1
        },
        info: [
            {
                id: 'id',
                value: '1406-I82193'
            },
            {
                id: 'date',
                value: '14.06.2019 18:12:13'
            },
            {
                id: 'tax',
                value: 'Тариф: 5%'
            },
            {
                id: 'status',
                value: 'Открыто'
            }
        ],
        amount: {
            value: 10433.35,
            currency: 'RUR',
            extra: ', к зачислению 96600000.00 RUR'
        },
        req: {
            value: 'Bitcoin, 1NeJEFzY8PbVS9RvYPfDP93iqXxHjav791',
            info: 'Lorem ipsum dolor sit amet consectetur adipisicing elit.'
        },
        actions: [
            {
                action: 'error',
                label: 'Сообщить об ошибке'
            }
        ]
    },
    {
        data: {
            status: 'open',
            course: '100345.32 USD',
            confirmations: 2
        },
        info: [
            {
                id: 'id',
                value: '1406-I82193'
            },
            {
                id: 'date',
                value: '14.06.2019 18:12:13'
            },
            {
                id: 'tax',
                value: 'Тариф 1%'
            },
            {
                id: 'status',
                value: 'Открыто'
            }
        ],
        amount: {
            value: 1015438.95,
            currency: 'USD',
            extra: ', к зачислению 96600000.00 RUR'
        },
        req: {
            value: 'DASH, 1NeJEFzY8PbVS9RvYPfDP93iqXxHjav791',
            info: 'Lorem ipsum dolor sit amet consectetur adipisicing elit.'
        },
        actions: [
            {
                action: 'error',
                label: 'Сообщить об ошибке'
            }
        ]
    },
    {
        data: {
            status: 'open',
            course: '10345.32 USD',
            confirmations: 1
        },
        info: [
            {
                id: 'id',
                value: '1406-I82193'
            },
            {
                id: 'date',
                value: '14.06.2019 18:12:13'
            },
            {
                id: 'tax',
                value: 'Тариф: 5%'
            },
            {
                id: 'status',
                value: 'Открыто'
            }
        ],
        amount: {
            value: 10433.35,
            currency: 'RUR',
            extra: ', к зачислению 96600000.00 RUR'
        },
        req: {
            value: 'Bitcoin, 1NeJEFzY8PbVS9RvYPfDP93iqXxHjav791',
            info: 'Lorem ipsum dolor sit amet consectetur adipisicing elit.'
        },
        actions: [
            {
                action: 'error',
                label: 'Сообщить об ошибке'
            }
        ]
    },
    {
        data: {
            status: 'open',
            course: '100345.32 USD',
            confirmations: 2
        },
        info: [
            {
                id: 'id',
                value: '1406-I82193'
            },
            {
                id: 'date',
                value: '14.06.2019 18:12:13'
            },
            {
                id: 'tax',
                value: 'Тариф 1%'
            },
            {
                id: 'status',
                value: 'Открыто'
            }
        ],
        amount: {
            value: 1015438.95,
            currency: 'USD',
            extra: ', к зачислению 96600000.00 RUR'
        },
        req: {
            value: 'DASH, 1NeJEFzY8PbVS9RvYPfDP93iqXxHjav791',
            info: 'Lorem ipsum dolor sit amet consectetur adipisicing elit.'
        },
        actions: [
            {
                action: 'error',
                label: 'Сообщить об ошибке'
            }
        ]
    }
];

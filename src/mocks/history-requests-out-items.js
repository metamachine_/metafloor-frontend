export default [
    {
        data: {
            status: 'open'
        },
        info: [
            {
                id: 'id',
                value: '1406-I82193'
            },
            {
                id: 'date',
                value: '14.06.2019 18:12:13'
            },
            {
                id: 'tax',
                value: 'Тариф: 4%'
            },
            {
                id: 'status',
                value: 'Открыто'
            }
        ],
        amount: {
            value: -10433.35,
            currency: 'RUR',
            extra: ', к зачислению 96600000.00 RUR'
        },
        req: {
            value: 'Наличными, курьером в кх в центре города Санкт-Петербург',
            info: 'Lorem ipsum dolor sit amet consectetur adipisicing elit.'
        },
        comment:
            'Обязательно сообщите как заберете! Санкт-Петербург, Невский проспект, 1, корп. 1 магазин Spar, шкаф 1-03, код 1234567890',
        actions: [
            {
                action: 'error',
                label: 'Сообщить об ошибке'
            },
            {
                action: 'accept',
                label: 'Подтвердить получение'
            }
        ]
    },
    {
        data: {
            status: 'close'
        },
        info: [
            {
                id: 'id',
                value: '1406-I82193'
            },
            {
                id: 'date',
                value: '14.06.2019 18:12:13'
            },
            {
                id: 'tax',
                value: 'Тариф 5%'
            },
            {
                id: 'status',
                value: 'Закрыт'
            }
        ],
        amount: {
            value: -1015438.95,
            currency: 'USD',
            extra: ', к зачислению 96600000.00 RUR'
        },
        req: {
            value: 'Наличными, курьером в кх в центре города Санкт-Петербург'
        },
        comment:
            'Обязательно сообщите как заберете! Санкт-Петербург, Невский проспект, 1, корп. 1 магазин Spar, шкаф 1-03, код 1234567890',
        actions: [
            {
                action: 'error',
                label: 'Сообщить об ошибке'
            },
            {
                action: 'accept',
                label: 'Подтвердить получение'
            }
        ]
    },
    {
        data: {
            status: 'open'
        },
        info: [
            {
                id: 'id',
                value: '1406-I82193'
            },
            {
                id: 'date',
                value: '14.06.2019 18:12:13'
            },
            {
                id: 'tax',
                value: 'Тариф: 4%'
            },
            {
                id: 'status',
                value: 'Открыто'
            }
        ],
        amount: {
            value: -10433.35,
            currency: 'RUR',
            extra: ', к зачислению 96600000.00 RUR'
        },
        req: {
            value: 'Наличными, курьером в кх в центре города Санкт-Петербург',
            info: 'Lorem ipsum dolor sit amet consectetur adipisicing elit.'
        },
        comment:
            'Обязательно сообщите как заберете! Санкт-Петербург, Невский проспект, 1, корп. 1 магазин Spar, шкаф 1-03, код 1234567890',
        actions: [
            {
                action: 'error',
                label: 'Сообщить об ошибке'
            },
            {
                action: 'accept',
                label: 'Подтвердить получение'
            }
        ]
    },
    {
        data: {
            status: 'close'
        },
        info: [
            {
                id: 'id',
                value: '1406-I82193'
            },
            {
                id: 'date',
                value: '14.06.2019 18:12:13'
            },
            {
                id: 'tax',
                value: 'Тариф 5%'
            },
            {
                id: 'status',
                value: 'Закрыт'
            }
        ],
        amount: {
            value: -1015438.95,
            currency: 'USD',
            extra: ', к зачислению 96600000.00 RUR'
        },
        req: {
            value: 'Наличными, курьером в кх в центре города Санкт-Петербург'
        },
        comment:
            'Обязательно сообщите как заберете! Санкт-Петербург, Невский проспект, 1, корп. 1 магазин Spar, шкаф 1-03, код 1234567890',
        actions: [
            {
                action: 'error',
                label: 'Сообщить об ошибке'
            },
            {
                action: 'accept',
                label: 'Подтвердить получение'
            }
        ]
    },
    {
        data: {
            status: 'open'
        },
        info: [
            {
                id: 'id',
                value: '1406-I82193'
            },
            {
                id: 'date',
                value: '14.06.2019 18:12:13'
            },
            {
                id: 'tax',
                value: 'Тариф: 4%'
            },
            {
                id: 'status',
                value: 'Открыто'
            }
        ],
        amount: {
            value: -10433.35,
            currency: 'RUR',
            extra: ', к зачислению 96600000.00 RUR'
        },
        req: {
            value: 'Наличными, курьером в кх в центре города Санкт-Петербург',
            info: 'Lorem ipsum dolor sit amet consectetur adipisicing elit.'
        },
        comment:
            'Обязательно сообщите как заберете! Санкт-Петербург, Невский проспект, 1, корп. 1 магазин Spar, шкаф 1-03, код 1234567890',
        actions: [
            {
                action: 'error',
                label: 'Сообщить об ошибке'
            },
            {
                action: 'accept',
                label: 'Подтвердить получение'
            }
        ]
    },
    {
        data: {
            status: 'close'
        },
        info: [
            {
                id: 'id',
                value: '1406-I82193'
            },
            {
                id: 'date',
                value: '14.06.2019 18:12:13'
            },
            {
                id: 'tax',
                value: 'Тариф 5%'
            },
            {
                id: 'status',
                value: 'Закрыт'
            }
        ],
        amount: {
            value: -1015438.95,
            currency: 'USD',
            extra: ', к зачислению 96600000.00 RUR'
        },
        req: {
            value: 'Наличными, курьером в кх в центре города Санкт-Петербург'
        },
        comment:
            'Обязательно сообщите как заберете! Санкт-Петербург, Невский проспект, 1, корп. 1 магазин Spar, шкаф 1-03, код 1234567890',
        actions: [
            {
                action: 'error',
                label: 'Сообщить об ошибке'
            },
            {
                action: 'accept',
                label: 'Подтвердить получение'
            }
        ]
    },
    {
        data: {
            status: 'open'
        },
        info: [
            {
                id: 'id',
                value: '1406-I82193'
            },
            {
                id: 'date',
                value: '14.06.2019 18:12:13'
            },
            {
                id: 'tax',
                value: 'Тариф: 4%'
            },
            {
                id: 'status',
                value: 'Открыто'
            }
        ],
        amount: {
            value: -10433.35,
            currency: 'RUR',
            extra: ', к зачислению 96600000.00 RUR'
        },
        req: {
            value: 'Наличными, курьером в кх в центре города Санкт-Петербург',
            info: 'Lorem ipsum dolor sit amet consectetur adipisicing elit.'
        },
        comment:
            'Обязательно сообщите как заберете! Санкт-Петербург, Невский проспект, 1, корп. 1 магазин Spar, шкаф 1-03, код 1234567890',
        actions: [
            {
                action: 'error',
                label: 'Сообщить об ошибке'
            },
            {
                action: 'accept',
                label: 'Подтвердить получение'
            }
        ]
    },
    {
        data: {
            status: 'close'
        },
        info: [
            {
                id: 'id',
                value: '1406-I82193'
            },
            {
                id: 'date',
                value: '14.06.2019 18:12:13'
            },
            {
                id: 'tax',
                value: 'Тариф 5%'
            },
            {
                id: 'status',
                value: 'Закрыт'
            }
        ],
        amount: {
            value: -1015438.95,
            currency: 'USD',
            extra: ', к зачислению 96600000.00 RUR'
        },
        req: {
            value: 'Наличными, курьером в кх в центре города Санкт-Петербург'
        },
        comment:
            'Обязательно сообщите как заберете! Санкт-Петербург, Невский проспект, 1, корп. 1 магазин Spar, шкаф 1-03, код 1234567890',
        actions: [
            {
                action: 'error',
                label: 'Сообщить об ошибке'
            },
            {
                action: 'accept',
                label: 'Подтвердить получение'
            }
        ]
    },
    {
        data: {
            status: 'open'
        },
        info: [
            {
                id: 'id',
                value: '1406-I82193'
            },
            {
                id: 'date',
                value: '14.06.2019 18:12:13'
            },
            {
                id: 'tax',
                value: 'Тариф: 4%'
            },
            {
                id: 'status',
                value: 'Открыто'
            }
        ],
        amount: {
            value: -10433.35,
            currency: 'RUR',
            extra: ', к зачислению 96600000.00 RUR'
        },
        req: {
            value: 'Наличными, курьером в кх в центре города Санкт-Петербург',
            info: 'Lorem ipsum dolor sit amet consectetur adipisicing elit.'
        },
        comment:
            'Обязательно сообщите как заберете! Санкт-Петербург, Невский проспект, 1, корп. 1 магазин Spar, шкаф 1-03, код 1234567890',
        actions: [
            {
                action: 'error',
                label: 'Сообщить об ошибке'
            },
            {
                action: 'accept',
                label: 'Подтвердить получение'
            }
        ]
    },
    {
        data: {
            status: 'close'
        },
        info: [
            {
                id: 'id',
                value: '1406-I82193'
            },
            {
                id: 'date',
                value: '14.06.2019 18:12:13'
            },
            {
                id: 'tax',
                value: 'Тариф 5%'
            },
            {
                id: 'status',
                value: 'Закрыт'
            }
        ],
        amount: {
            value: -1015438.95,
            currency: 'USD',
            extra: ', к зачислению 96600000.00 RUR'
        },
        req: {
            value: 'Наличными, курьером в кх в центре города Санкт-Петербург'
        },
        comment:
            'Обязательно сообщите как заберете! Санкт-Петербург, Невский проспект, 1, корп. 1 магазин Spar, шкаф 1-03, код 1234567890',
        actions: [
            {
                action: 'error',
                label: 'Сообщить об ошибке'
            },
            {
                action: 'accept',
                label: 'Подтвердить получение'
            }
        ]
    },
    {
        data: {
            status: 'open'
        },
        info: [
            {
                id: 'id',
                value: '1406-I82193'
            },
            {
                id: 'date',
                value: '14.06.2019 18:12:13'
            },
            {
                id: 'tax',
                value: 'Тариф: 4%'
            },
            {
                id: 'status',
                value: 'Открыто'
            }
        ],
        amount: {
            value: -10433.35,
            currency: 'RUR',
            extra: ', к зачислению 96600000.00 RUR'
        },
        req: {
            value: 'Наличными, курьером в кх в центре города Санкт-Петербург',
            info: 'Lorem ipsum dolor sit amet consectetur adipisicing elit.'
        },
        comment:
            'Обязательно сообщите как заберете! Санкт-Петербург, Невский проспект, 1, корп. 1 магазин Spar, шкаф 1-03, код 1234567890',
        actions: [
            {
                action: 'error',
                label: 'Сообщить об ошибке'
            },
            {
                action: 'accept',
                label: 'Подтвердить получение'
            }
        ]
    },
    {
        data: {
            status: 'close'
        },
        info: [
            {
                id: 'id',
                value: '1406-I82193'
            },
            {
                id: 'date',
                value: '14.06.2019 18:12:13'
            },
            {
                id: 'tax',
                value: 'Тариф 5%'
            },
            {
                id: 'status',
                value: 'Закрыт'
            }
        ],
        amount: {
            value: -1015438.95,
            currency: 'USD',
            extra: ', к зачислению 96600000.00 RUR'
        },
        req: {
            value: 'Наличными, курьером в кх в центре города Санкт-Петербург'
        },
        comment:
            'Обязательно сообщите как заберете! Санкт-Петербург, Невский проспект, 1, корп. 1 магазин Spar, шкаф 1-03, код 1234567890',
        actions: [
            {
                action: 'error',
                label: 'Сообщить об ошибке'
            },
            {
                action: 'accept',
                label: 'Подтвердить получение'
            }
        ]
    },
    {
        data: {
            status: 'open'
        },
        info: [
            {
                id: 'id',
                value: '1406-I82193'
            },
            {
                id: 'date',
                value: '14.06.2019 18:12:13'
            },
            {
                id: 'tax',
                value: 'Тариф: 4%'
            },
            {
                id: 'status',
                value: 'Открыто'
            }
        ],
        amount: {
            value: -10433.35,
            currency: 'RUR',
            extra: ', к зачислению 96600000.00 RUR'
        },
        req: {
            value: 'Наличными, курьером в кх в центре города Санкт-Петербург',
            info: 'Lorem ipsum dolor sit amet consectetur adipisicing elit.'
        },
        comment:
            'Обязательно сообщите как заберете! Санкт-Петербург, Невский проспект, 1, корп. 1 магазин Spar, шкаф 1-03, код 1234567890',
        actions: [
            {
                action: 'error',
                label: 'Сообщить об ошибке'
            },
            {
                action: 'accept',
                label: 'Подтвердить получение'
            }
        ]
    },
    {
        data: {
            status: 'close'
        },
        info: [
            {
                id: 'id',
                value: '1406-I82193'
            },
            {
                id: 'date',
                value: '14.06.2019 18:12:13'
            },
            {
                id: 'tax',
                value: 'Тариф 5%'
            },
            {
                id: 'status',
                value: 'Закрыт'
            }
        ],
        amount: {
            value: -1015438.95,
            currency: 'USD',
            extra: ', к зачислению 96600000.00 RUR'
        },
        req: {
            value: 'Наличными, курьером в кх в центре города Санкт-Петербург'
        },
        comment:
            'Обязательно сообщите как заберете! Санкт-Петербург, Невский проспект, 1, корп. 1 магазин Spar, шкаф 1-03, код 1234567890',
        actions: [
            {
                action: 'error',
                label: 'Сообщить об ошибке'
            },
            {
                action: 'accept',
                label: 'Подтвердить получение'
            }
        ]
    },
    {
        data: {
            status: 'open'
        },
        info: [
            {
                id: 'id',
                value: '1406-I82193'
            },
            {
                id: 'date',
                value: '14.06.2019 18:12:13'
            },
            {
                id: 'tax',
                value: 'Тариф: 4%'
            },
            {
                id: 'status',
                value: 'Открыто'
            }
        ],
        amount: {
            value: -10433.35,
            currency: 'RUR',
            extra: ', к зачислению 96600000.00 RUR'
        },
        req: {
            value: 'Наличными, курьером в кх в центре города Санкт-Петербург',
            info: 'Lorem ipsum dolor sit amet consectetur adipisicing elit.'
        },
        comment:
            'Обязательно сообщите как заберете! Санкт-Петербург, Невский проспект, 1, корп. 1 магазин Spar, шкаф 1-03, код 1234567890',
        actions: [
            {
                action: 'error',
                label: 'Сообщить об ошибке'
            },
            {
                action: 'accept',
                label: 'Подтвердить получение'
            }
        ]
    },
    {
        data: {
            status: 'close'
        },
        info: [
            {
                id: 'id',
                value: '1406-I82193'
            },
            {
                id: 'date',
                value: '14.06.2019 18:12:13'
            },
            {
                id: 'tax',
                value: 'Тариф 5%'
            },
            {
                id: 'status',
                value: 'Закрыт'
            }
        ],
        amount: {
            value: -1015438.95,
            currency: 'USD',
            extra: ', к зачислению 96600000.00 RUR'
        },
        req: {
            value: 'Наличными, курьером в кх в центре города Санкт-Петербург'
        },
        comment:
            'Обязательно сообщите как заберете! Санкт-Петербург, Невский проспект, 1, корп. 1 магазин Spar, шкаф 1-03, код 1234567890',
        actions: [
            {
                action: 'error',
                label: 'Сообщить об ошибке'
            },
            {
                action: 'accept',
                label: 'Подтвердить получение'
            }
        ]
    },
    {
        data: {
            status: 'open'
        },
        info: [
            {
                id: 'id',
                value: '1406-I82193'
            },
            {
                id: 'date',
                value: '14.06.2019 18:12:13'
            },
            {
                id: 'tax',
                value: 'Тариф: 4%'
            },
            {
                id: 'status',
                value: 'Открыто'
            }
        ],
        amount: {
            value: -10433.35,
            currency: 'RUR',
            extra: ', к зачислению 96600000.00 RUR'
        },
        req: {
            value: 'Наличными, курьером в кх в центре города Санкт-Петербург',
            info: 'Lorem ipsum dolor sit amet consectetur adipisicing elit.'
        },
        comment:
            'Обязательно сообщите как заберете! Санкт-Петербург, Невский проспект, 1, корп. 1 магазин Spar, шкаф 1-03, код 1234567890',
        actions: [
            {
                action: 'error',
                label: 'Сообщить об ошибке'
            },
            {
                action: 'accept',
                label: 'Подтвердить получение'
            }
        ]
    },
    {
        data: {
            status: 'close'
        },
        info: [
            {
                id: 'id',
                value: '1406-I82193'
            },
            {
                id: 'date',
                value: '14.06.2019 18:12:13'
            },
            {
                id: 'tax',
                value: 'Тариф 5%'
            },
            {
                id: 'status',
                value: 'Закрыт'
            }
        ],
        amount: {
            value: -1015438.95,
            currency: 'USD',
            extra: ', к зачислению 96600000.00 RUR'
        },
        req: {
            value: 'Наличными, курьером в кх в центре города Санкт-Петербург'
        },
        comment:
            'Обязательно сообщите как заберете! Санкт-Петербург, Невский проспект, 1, корп. 1 магазин Spar, шкаф 1-03, код 1234567890',
        actions: [
            {
                action: 'error',
                label: 'Сообщить об ошибке'
            },
            {
                action: 'accept',
                label: 'Подтвердить получение'
            }
        ]
    },
    {
        data: {
            status: 'open'
        },
        info: [
            {
                id: 'id',
                value: '1406-I82193'
            },
            {
                id: 'date',
                value: '14.06.2019 18:12:13'
            },
            {
                id: 'tax',
                value: 'Тариф: 4%'
            },
            {
                id: 'status',
                value: 'Открыто'
            }
        ],
        amount: {
            value: -10433.35,
            currency: 'RUR',
            extra: ', к зачислению 96600000.00 RUR'
        },
        req: {
            value: 'Наличными, курьером в кх в центре города Санкт-Петербург',
            info: 'Lorem ipsum dolor sit amet consectetur adipisicing elit.'
        },
        comment:
            'Обязательно сообщите как заберете! Санкт-Петербург, Невский проспект, 1, корп. 1 магазин Spar, шкаф 1-03, код 1234567890',
        actions: [
            {
                action: 'error',
                label: 'Сообщить об ошибке'
            },
            {
                action: 'accept',
                label: 'Подтвердить получение'
            }
        ]
    },
    {
        data: {
            status: 'close'
        },
        info: [
            {
                id: 'id',
                value: '1406-I82193'
            },
            {
                id: 'date',
                value: '14.06.2019 18:12:13'
            },
            {
                id: 'tax',
                value: 'Тариф 5%'
            },
            {
                id: 'status',
                value: 'Закрыт'
            }
        ],
        amount: {
            value: -1015438.95,
            currency: 'USD',
            extra: ', к зачислению 96600000.00 RUR'
        },
        req: {
            value: 'Наличными, курьером в кх в центре города Санкт-Петербург'
        },
        comment:
            'Обязательно сообщите как заберете! Санкт-Петербург, Невский проспект, 1, корп. 1 магазин Spar, шкаф 1-03, код 1234567890',
        actions: [
            {
                action: 'error',
                label: 'Сообщить об ошибке'
            },
            {
                action: 'accept',
                label: 'Подтвердить получение'
            }
        ]
    }
];

import Vue from 'vue';
import Vuex from 'vuex';

import core from '@/core/store';
import modules from './modules';

Vue.use(Vuex);

const store = new Vuex.Store({
    modules: { core, ...modules }
});

export default store;

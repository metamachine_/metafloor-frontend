import { init } from '@/interfaces/api';

/**
 * @module store/app
 * @desc общий vuex модуль для работы с приложением
 */
export const namespaced = true;

export const state = {
    /**
     * @name state/initialized
     * @see {@link module:store/app~getter/initialized}
     */
    initialized: false
};

export const getters = {
    /**
     * @name getter/initialized
     * @desc Инициализированно ли приложение (выполнены все методы для первой отрисовки)
     *
     * @type Boolean
     *
     * @example
     * ...mapGetters({
     *      initialized: 'app/initialized'
     * })
     */
    initialized: (state) => state.initialized
};

export const mutations = {
    SET_INITIALIZED(state) {
        state.initialized = true;
    }
};

export const actions = {
    setInitialized(store) {
        store.commit('SET_INITIALIZED');
    },
    /**
     * @function action/init
     * @desc Используй этот метод для инициализации приложения
     * <br>
     * (первичные http запросы, сокеты, вызов других методов для инициализации и т.д.)
     * <br>
     * <br>
     * Этот метод вызывается единожды в {@link module:middlewares.mainBefore}
     */
    init(store) {
        return new Promise((resolve, reject) => {
            init().then(({ status, data }) => {
                resolve();
            });
        });
    }
};

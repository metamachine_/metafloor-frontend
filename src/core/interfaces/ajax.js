import { BACKEND_HOST } from '@/consts';
import axios from 'axios';
import Cookies from 'js-cookie';

/**
 * @module core/interfaces/ajax
 * @desc Интерфейс для работы с AJAX (backend) запросами
 *
 * @see {@link https://github.com/axios/axios}
 *
 * @example
 * import { API_PREFIX } from '@/consts';
 * import path from 'path';
 * import ajax from '@/core/interfaces/ajax';
 *
 * const apiPath = path.join(API_PREFIX, '/some-api-endpoint');
 * ajax.post(apiPath, someData); //promise
 */

let baseURL = BACKEND_HOST;
if (process.env.NODE_ENV === 'development') {
    baseURL = Cookies.get('dev-backend-host') || BACKEND_HOST;
}

const ajax = axios.create({
    baseURL,
    withCredentials: true,
    timeout: 10000,
    xsrfCookieName: 'excsrftoken',
    xsrfHeaderName: 'x-csrftoken'
});

export default ajax;

export const namespaced = true;

export const state = {
    queue: 0
};

export const getters = {
    isLoading: (state) => state.queue > 0
};

export const mutations = {
    ADD_QUEUE(state) {
        state.queue += 1;
    },
    REMOVE_QUEUE(state) {
        state.queue -= 1;
    }
};

export const actions = {
    show({ commit }) {
        commit('ADD_QUEUE');
    },
    hide({ commit }) {
        commit('REMOVE_QUEUE');
    }
};

import { uniqueId } from '@/core/modules';

export const namespaced = true;

export const state = {
    notifications: []
};

export const getters = {
    notifications: (state) => state.notifications,
    findNotificationIndexById: (state) => (id) => {
        const index = state.notifications.findIndex((notification) => notification.id === id);
        return index;
    }
};

export const mutations = {
    PUSH(state, { title, text, color, icon, id, timeout }) {
        state.notifications.push({
            title,
            text,
            color,
            icon,
            id,
            timeout
        });
    },
    HIDE(state, index) {
        if (state.notifications[index]) {
            if (state.notifications[index].timeout) {
                clearTimeout(state.notifications[index].timeout);
            }

            state.notifications.splice(index, 1);
        }
    }
};

export const actions = {
    push({ commit, dispatch }, { title, text, color = 'default', icon = true, timeout }) {
        const id = uniqueId();

        if (!timeout) {
            timeout = text.length * 100 + 1000;
        }

        const $timeout = setTimeout(() => {
            dispatch('hide', id);
        }, timeout);

        commit('PUSH', {
            title,
            text,
            color,
            icon,
            id,
            timeout: $timeout
        });
    },
    hide({ getters, commit }, id) {
        const index = getters.findNotificationIndexById(id);
        if (index != -1) {
            commit('HIDE', index);
        }
    }
};

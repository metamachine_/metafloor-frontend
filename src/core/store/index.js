import * as notifications from './notifications';
import * as pageLoading from './page-loading';

export default {
    namespaced: true,
    modules: {
        notifications,
        pageLoading
    }
};

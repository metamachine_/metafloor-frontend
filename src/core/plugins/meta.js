import Vue from 'vue';
import VueMeta from 'vue-meta';

/**
 * @module core/plugins/meta
 * @see {@link https://github.com/nuxt/vue-meta}
 */
Vue.use(VueMeta);

import { getValueByStringKey } from '@/core/modules';

/**
 * @module core/directives/is-valid
 * @desc Директива для обозначения валидно поле или нет на основе v-model
 *
 * @see {@link module:core/mixins/validation}
 */
export default {
    bind($el, { value, modifiers }, vNode) {
        const modelExpression = vNode.data.model ? vNode.data.model.expression : '';
        const customEvents = Object.keys(modifiers);
        const bindingEvents = customEvents.length ? customEvents : ['input', 'change', 'focusout'];
        const hasFocusOutEvent = bindingEvents.includes('focusout');
        let modelStringKey;

        if (value) {
            modelStringKey = value;
        }

        if (!modelStringKey && modelExpression) {
            modelStringKey = modelExpression;
        }

        if (!modelStringKey) {
            return;
        }

        const handler = (e) => {
            const $v = getValueByStringKey(vNode.context.$v, modelStringKey);

            if ($v) {
                if (hasFocusOutEvent) {
                    if (e.type !== 'input') {
                        $v.$touch();
                    }
                } else {
                    $v.$touch();
                }

                setTimeout(() => {
                    if ($v.$error) {
                        $el.classList.add('validation--invalid');
                    } else {
                        $el.classList.remove('validation--invalid');
                    }
                    if (!$v.$invalid) {
                        $el.classList.add('validation--valid');
                    } else {
                        $el.classList.remove('validation--valid');
                    }
                });
            }
        };

        $el.__is_valid_model_string_key__ = modelStringKey;
        $el.__is_valid_binding_events__ = bindingEvents;
        $el.__is_valid_fn__ = (e) => handler(e);

        bindingEvents.forEach((eventName) => {
            $el.addEventListener(eventName, $el.__is_valid_fn__);
        });
    },
    update($el, {}, vNode) {
        if ($el.__is_valid_model_string_key__) {
            const $v = getValueByStringKey(vNode.context.$v, $el.__is_valid_model_string_key__);

            if ($v) {
                setTimeout(() => {
                    if (!$v.$dirty) {
                        $el.classList.remove('validation--invalid');
                        $el.classList.remove('validation--valid');
                    } else {
                        if ($v.$error) {
                            $el.classList.add('validation--invalid');
                        } else {
                            $el.classList.remove('validation--invalid');
                        }
                        if (!$v.$invalid) {
                            $el.classList.add('validation--valid');
                        } else {
                            $el.classList.remove('validation--valid');
                        }
                    }
                });
            }
        }
    },
    unbind($el) {
        if ($el.__is_valid_binding_events__) {
            $el.__is_valid_binding_events__.forEach((eventName) => {
                $el.removeEventListener(eventName, $el.__is_valid_fn__);
            });
        }
    }
};

import * as clipboard from 'clipboard-polyfill';
import store from '@/store';

const getData = ($el, value) => {
    const $target = document.getElementById(value.target);
    $el.__copy_target__ = $target;
    $el.__copy_text__ = $target ? $target.value || value.text : value.text;
    $el.__copy_callback__ = value.callback;
    $el.__copy_notification__ = value.notification;
};

/**
 * @module directives/copy
 * @desc  Директива для копирования в буфер обмена при клике
 *
 * @prop {} text - значение для копирования
 * @prop {String} target - id элемента (инпута) для копирования
 * @prop {Function} [callback]
 *
 * @example
 * {@lang xml}
 * <!--[text]-->
 * <button class="example-copy-element"
 *         v-copy="{ text: 'copy that', callback: onCopy }">
 *         click to copy
 * </button>
 *
 * <!--[input]-->
 * <input id="copy-input" value="copy that">
 * <button class="example-copy-element"
 *         v-copy="{ target: 'copy-input', callback: onCopy }">
 *         click to copy
 * </button>
 *
 * @example
 * import copy from '@/directives/copy';
 *
 * export default {
 *      directives: {
 *          copy
 *      }
 * }
 */
export default {
    inserted($el, { value }) {
        getData($el, value);

        const copy = () => {
            const text = $el.__copy_text__;

            if (text) {
                const $target = $el.__copy_target__;
                const callback = $el.__copy_callback__;
                const notification = $el.__copy_notification__;

                clipboard
                    .writeText(text)
                    .then(() => {
                        const defaultNotification = store.getters['core/lang/i18n']('copy_notification');
                        store.dispatch('core/notifications/push', {
                            text: notification || defaultNotification
                        });
                        if (callback instanceof Function) {
                            callback({ el: $el, target: $target, text });
                        }
                    })
                    .catch(console.error);
            }
        };

        $el.__copy_fn__ = () => copy();

        $el.addEventListener('click', $el.__copy_fn__);
    },
    update($el, { value }) {
        getData($el, value);
    },
    unbind($el) {
        $el.removeEventListener('click', $el.__copy_fn__);
    }
};

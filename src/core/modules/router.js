import { uniqueId } from './index';

export const eachRoute = (route, cb) => {
    if (route && route.matched) {
        route.matched.forEach((_route) => {
            cb(_route);
        });
    }
};

export const eachRouteMeta = (route, cb) => {
    eachRoute(route, (_route) => {
        cb(_route.meta || {});
    });
};

export const getRoutePath = (router, name) => {
    return router.match({ name }).path;
};

export const applyBeforeEachRouter = (to, from, next, name) => {
    const methods = [];

    eachRouteMeta(to, (meta) => {
        if (meta[name]) {
            meta[name].forEach((method) => methods.push(method.call(null, to, from)));
        }
    });

    Promise.all(methods)
        .then((data) => {
            next(data[data.length - 1]);
        })
        .catch((res) => {
            if (!res) {
                res = {
                    name: 'error'
                };
            }
            res.query = {
                __: uniqueId()
            };
            next(res);
        });
};

export const applyAllMiddlewareRouter = (to, from, next) => {
    applyBeforeEachRouter(to, from, next, 'middleware');
};

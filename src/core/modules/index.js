/**
 * @module core/modules/index
 * @desc Общие вспомогательные модули
 */

let _uid = 0;

/**
 * @function
 * @desc Генерирует уникальный id (в контексте процесса)
 *
 * @return {Number}
 *
 * @example
 * import { uniqueId } from '@/core/modules';
 * uniqueId(); //1
 * uniqueId(); //2
 * uniqueId(); //3
 */
export const uniqueId = () => {
    return _uid++;
};

/**
 * @function
 * @desc Ищет значение из объекта
 *
 * @param {Object} obj - объект
 * @param {String} stringKey - путь до свойства
 *
 * @return {}
 *
 * @example
 * import { getValueByStringKey } from '@/core/modules';
 * const obj = { some: { nested: { example: 'test' } } };
 * getValueByStringKey(obj, 'some.nested.example'); //test
 */
export const getValueByStringKey = (obj, stringKey) => {
    stringKey = stringKey.replace(/\[(\w+)\]/g, '.$1');
    stringKey = stringKey.replace(/^\./, '');
    const a = stringKey.split('.');

    for (let i = 0, n = a.length; i < n; ++i) {
        const k = a[i];
        if (k in obj) {
            obj = obj[k];
        } else {
            return;
        }
    }
    return obj;
};

/**
 * @function
 * @desc Возвращает строку в snake_case
 *
 * @param {String} string
 *
 * @return {String}
 *
 * @example
 * import { toSnakeCase } from '@/core/modules';
 * toSnakeCase('In queue'); // in_queue
 */
export const toSnakeCase = (string) => string.toLowerCase().replace(/\s/g, '_');

/**
 * @function
 * @desc Поиск родительского элемента по элементу
 *
 * @param {HTMLElement} $el - начальный элемент
 * @param {HTMLElement} $parentEl - искомый элемент
 * @param {Boolean} [self=true] - считать ли начальный элемент искомым
 *
 * @return {HTMLElement|null}
 *
 * @example
 * import { findParentByElement } from '@/core/modules';
 * const $child = document.getElementById('child');
 * const $parent = document.getElementById('parent-of-child');
 * const $notParent = document.getElementById('not-parent-of-child')
 *
 * findParentByElement($child, $parent); // $parent
 * findParentByElement($child, $notParent); // null
 */
export const findParentByElement = ($el, $parentEl, self = true) => {
    if (!$el) {
        return null;
    }

    if (self && $el === $parentEl) {
        return $el;
    }

    while (($el = $el.parentElement) && $el !== $parentEl);
    return $el;
};

/**
 * @function
 * @desc Вернёт новый объект с извлечёными свойствами
 *
 * @param {Object} obj - объект
 * @param {String[]} props - свойства для исключения
 *
 * @return {Object} - новый объект
 *
 * @example
 * import { getExcludedObjectByProps } from '@/core/modules';
 * const obj = { test: 'foo', exclude_me: 'bar' };
 * getExcludedObjectByProps(obj, ['exclude_me']); // { test: 'foo' }
 */
export const getExcludedObjectByProps = (obj, props = []) => {
    obj = { ...obj };

    props.forEach((prop) => {
        if (obj[prop]) {
            delete obj[prop];
        }
    });

    return obj;
};

/**
 * @function
 * @desc Сравнение двух массивов
 *
 * @param {Array} arr1 - первый массив
 * @param {Array} arr2 - второй массив
 *
 * @return {Boolean}
 * @example
 * import { isEqualArray }  from '@/core/modules';
 * isEqualArray([1, 1], [1, 1]); // true
 * isEqualArray([1, 2], [1, 1]); // false
 */
export const isEqualArray = (arr1, arr2) => {
    if (!arr1 || !arr2) return;

    const result = arr1.every((e1, i) => {
        const e2 = arr2[i];
        if (e1 instanceof Array && e1.length > 1 && e2.length) {
            return isEqualArray(e1, e2);
        } else if (e1 !== e2) {
            return false;
        } else {
            return true;
        }
    });

    return result;
};

export const getCssVar = (name) => {
    const cssVar = getComputedStyle(document.documentElement).getPropertyValue(`--${name}`);
    return cssVar;
};

export const parseDuration = (cssDuration) => {
    const parsedDuration = parseFloat(cssDuration) * 1000;
    return parsedDuration;
};

export const getTransitionDuration = () => {
    const transitionDuration = parseDuration(getCssVar('transition-duration'));
    return transitionDuration;
};

export const generatePagination = (c, m) => {
    const current = c;
    const last = m;
    const delta = 2;
    const left = current - delta;
    const right = current + delta + 1;
    const range = [];
    const rangeWithDots = [];
    let l;

    for (let i = 1; i <= last; i++) {
        if (i == 1 || i == last || (i >= left && i < right)) {
            range.push(i);
        }
    }

    for (const i of range) {
        if (l) {
            if (i - l === 2) {
                rangeWithDots.push(l + 1);
            } else if (i - l !== 1) {
                rangeWithDots.push('...');
            }
        }
        rangeWithDots.push(i);
        l = i;
    }

    return rangeWithDots;
};

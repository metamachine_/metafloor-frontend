/**
 * @const
 * @type {String}
 * @desc Домен бекенда
 *
 * @example
 * import { BACKEND_HOST } from '@/consts';
 */
export const BACKEND_HOST = (() => {
    if (process.env.NODE_ENV === 'development') {
        if (process.env.VUE_APP_BACKEND_HOST === '' || process.env.VUE_APP_BACKEND_HOST === '/') {
            return window.location.origin;
        } else {
            return process.env.VUE_APP_BACKEND_HOST;
        }
    }

    if (process.env.NODE_ENV === 'production') {
        return window.location.origin;
    }
})();

/**
 * @const
 * @type {String}
 * @desc Префикс пути (url) для API запросов
 *
 * @example
 * import { API_PREFIX } from '@/consts';
 */
export const API_PREFIX = '/ajax';

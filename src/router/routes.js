const Index = () => import('@/pages/index.vue' /* webpackChunkName: 'pages/index' */);
const Faq = () => import('@/pages/faq.vue' /* webpackChunkName: 'pages/faq' */);

/**
 * @module router/routes
 * @desc Список роутов
 *
 * @prop {Array} [meta.middleware=[]] - мидлвейры
 * @prop {Boolean} [meta.saveScrollPosition=false] - сохранять ли позицию скролла
 * @prop {String} [meta.layout=default] - лэйаут
 *
 * @see {@link module:middlewares}
 */
const routes = [
    {
        name: 'home',
        path: '/',
        component: Index
    },
    {
        name: 'faq',
        path: '/faq',
        component: Faq
    }
];

export default routes;

import Vue from 'vue';
import VueRouter from 'vue-router';
import { applyAllMiddlewareRouter } from '@/core/modules/router';

import { mainBefore as mainBeforeMiddleware, mainAfter as mainAfterMiddleware } from '@/middlewares';
import routes from './routes';

Vue.use(VueRouter);

const router = new VueRouter({
    mode: 'history',
    routes: routes,
    linkActiveClass: '',
    linkExactActiveClass: 'router--active'
});

router.beforeEach(mainBeforeMiddleware);
router.beforeEach(applyAllMiddlewareRouter);
router.afterEach(mainAfterMiddleware);

export default router;

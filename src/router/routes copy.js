const Index = () => import('@/pages/index.vue' /* webpackChunkName: 'pages/index' */);

/**
 * @module router/routes
 * @desc Список роутов
 *
 * @prop {Array} [meta.middleware=[]] - мидлвейры
 * @prop {Boolean} [meta.saveScrollPosition=false] - сохранять ли позицию скролла
 * @prop {String} [meta.layout=default] - лэйаут
 *
 * @see {@link module:middlewares}
 */
const routes = [
    {
        name: 'home',
        path: '/',
        component: Index,
        meta: {
            news: true
        }
    }
];

export default routes;

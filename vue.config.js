const path = require('path');

module.exports = {
    chainWebpack: (config) => {
        config.resolve.alias.set('@', path.resolve(__dirname, 'src/'));
        const types = ['vue-modules', 'vue', 'normal-modules', 'normal'];
        types.forEach((type) => addStyleResource(config.module.rule('stylus').oneOf(type)));

        // https://github.com/vuejs/vue-cli/issues/1132
        if (process.env.NODE_ENV === 'development') {
            config.output.filename('[name].[hash].js').end();
        }

        /*
        config.module.rule('svg').exclude.add(/assets\/icons/);
        config.module
            .rule('svg-icon')
            .test(/\.(svg)(\?.*)?$/)
            .include.add(/assets\/icons/)
            .end()
            .use('html-loader')
            .loader('html-loader');
        */
    },
    pages: {
        index: {
            entry: './src/app.js'
        }
    },
    devServer: {
        https: false,
        host: '0.0.0.0',
        disableHostCheck: true,
        allowedHosts: ['.localhost', '.localhost:8080']
    },
    productionSourceMap: false,

    lintOnSave: false,
    baseUrl: undefined,
    outputDir: undefined,
    runtimeCompiler: true,
    parallel: undefined
};

function addStyleResource(rule) {
    rule.use('style-resource')
        .loader('style-resources-loader')
        .options({
            patterns: [
                path.resolve(__dirname, './src/stylesheets/vars.styl'),
                path.resolve(__dirname, './src/stylesheets/mixins.styl')
            ]
        });
}

image: registry01.aps.lan:5000/aps/aps-frontend/aps-frontend-ci:centos7

before_script:
   - 'echo -e "\033[37;1;42m Building: \033[0m\033[37;1;10m ${CI_COMMIT_SHA:0:8} \033[0m"'

stages:
   - build_ci
   - lint
   - build
   - build_rpm
   - deploy_nexus_stage
   - deploy_nexus_prod
   - deploy_dev
   - deploy_stage
   - deploy_prod
   - tests

build_ci_docker_image:
   image: registry01.aps.lan:5000/ops/gitlab-ci:docker-dind
   stage: build_ci
   allow_failure: false
   tags:
      - docker-ci
   services:
      - docker:dind
   before_script:
      - docker login -u gitlab-ci-token -p ${CI_BUILD_TOKEN} registry01.aps.lan:5000
      - . ${CI_PROJECT_DIR}/contrib/ci/docker-ci.ver
      - 'echo -e "\033[37;1;42m Building: \033[0m\033[37;1;10m aps-frontend-ci:centos7 ver: ${CI_DOCKER_IMAGE_VERSION} \033[0m"'
   script:
      - docker build --rm -t registry01.aps.lan:5000/aps/aps-frontend/aps-frontend-ci:centos7 ${CI_PROJECT_DIR}/contrib/ci
      - docker push  registry01.aps.lan:5000/aps/aps-frontend/aps-frontend-ci:centos7
   only:
      changes:
         - contrib/ci/docker-ci.ver

lint_rpm:
   stage: lint
   allow_failure: false
   tags:
      - docker-dev
   script:
      - rpmlint ${CI_PROJECT_DIR}/contrib/rpm/aps-front.spec
   only:
      - master
      - develop

build_front_prod:
   stage: build
   allow_failure: false
   tags:
      - docker-prod
   before_script:
      - 'echo -e "\033[37;1;42m Building: \033[0m\033[37;1;10m front ${CI_COMMIT_SHA:0:8} \033[0m"'
   script:
      - cd ${CI_PROJECT_DIR} && yarn
      - cd ${CI_PROJECT_DIR} && npm run build
   artifacts:
      paths:
         - ${CI_PROJECT_DIR}/dist/
      expire_in: 3 days
      name: "aps-front-${CI_COMMIT_SHA:0:8}"
   only:
      - master

build_front:
   stage: build
   allow_failure: false
   tags:
      - docker-dev
   before_script:
      - 'echo -e "\033[37;1;42m Building: \033[0m\033[37;1;10m front ${CI_COMMIT_SHA:0:8} \033[0m"'
   script:
      - cd ${CI_PROJECT_DIR} && yarn
      - cd ${CI_PROJECT_DIR} && npm run build-dev
   artifacts:
      paths:
         - ${CI_PROJECT_DIR}/dist/
      expire_in: 3 days
      name: "aps-front-${CI_COMMIT_SHA:0:8}"
   except:
      - master

build_rpm:
   stage: build_rpm
   allow_failure: false
   tags:
      - docker-dev
   before_script:
      - export CI_BRANCH=${CI_COMMIT_REF_NAME}
      - export CI_REPO_NAME='aps-stage'
      - export CI_PKG_NAME="aps-front"
      - export CI_PKG_VERSION=$(git describe --abbrev=0 --tags | sed 's/[^0-9\.]//g')
      - export CI_PKG_RELEASE=$(${CI_PROJECT_DIR}/contrib/ci/version.py)
      - export CI_PACKAGE_FILE="${CI_PKG_NAME}-${CI_PKG_VERSION}-${CI_PKG_RELEASE}.${CI_COMMIT_SHA:0:8}.el7.noarch.rpm"
      - 'echo -e "\033[37;1;42m Building: \033[0m\033[37;1;10m ${CI_PACKAGE_FILE} \033[0m"'
   script:
      - mkdir ${CI_PROJECT_DIR}/rpmbuild
      - rpmbuild -bb --define "_topdir ${CI_PROJECT_DIR}/rpmbuild/"
                     --define "_sourcedir ${CI_PROJECT_DIR}/"
                     --define "ci_branch ${CI_BRANCH}"
                     --define "ci_commit_id ${CI_COMMIT_SHA:0:8}"
                     --define "pkg_version ${CI_PKG_VERSION}"
                     --define "pkg_release ${CI_PKG_RELEASE}"
                     ${CI_PROJECT_DIR}/contrib/rpm/aps-front.spec
   artifacts:
      paths:
         - ${CI_PROJECT_DIR}/rpmbuild/RPMS/noarch/${CI_PACKAGE_FILE}
      expire_in: 3 days
      name: "${CI_PKG_NAME}-${CI_COMMIT_SHA:0:8}"
   only:
      - master
      - develop

deploy_rpm_nexus_stage:
   stage: deploy_nexus_stage
   allow_failure: false
   tags:
      - docker-dev
   before_script:
      - export CI_BRANCH=${CI_COMMIT_REF_NAME}
      - export CI_REPO_NAME='aps-stage'
      - export CI_PKG_NAME="aps-front"
      - export CI_PKG_VERSION=$(git describe --abbrev=0 --tags | sed 's/[^0-9\.]//g')
      - export CI_PKG_RELEASE=$(${CI_PROJECT_DIR}/contrib/ci/version.py)
      - export CI_PACKAGE_FILE="${CI_PKG_NAME}-${CI_PKG_VERSION}-${CI_PKG_RELEASE}.${CI_COMMIT_SHA:0:8}.el7.noarch.rpm"
      - 'echo -e "\033[37;1;42m Building: \033[0m\033[37;1;10m ${CI_PACKAGE_FILE} \033[0m"'
   script:
      - curl -v --user "${CI_NEXUS_STAGE_USER}:${CI_NEXUS_STAGE_PASSWD}"
             --upload-file ${CI_PROJECT_DIR}/rpmbuild/RPMS/noarch/${CI_PACKAGE_FILE}
                           http://repo.aps.lan/nexus/repository/aps-stage/centos/7/x86_64/${CI_PACKAGE_FILE}
      - echo "${CI_PACKAGE_FILE}">${CI_PROJECT_DIR}/deployed.pkg
   artifacts:
      paths:
         - ${CI_PROJECT_DIR}/deployed.pkg
      name: deployed.pkg
   environment:
      name: staging
      url: https://aps.lan
   only:
      - master
      - develop

deploy_rpm_nexus_prod:
   stage: deploy_nexus_prod
   allow_failure: false
   tags:
      - docker-prod
   script:
      - CI_PACKAGE_FILE=$(cat ${CI_PROJECT_DIR}/deployed.pkg)
      - curl http://repo.aps.lan/nexus/repository/aps-stage/centos/7/x86_64/${CI_PACKAGE_FILE} |
        curl -v --user "${CI_NEXUS_PROD_USER}:${CI_NEXUS_PROD_PASSWD}"
             --upload-file - http://repo.aps.lan/nexus/repository/aps/centos/7/x86_64/${CI_PACKAGE_FILE}
   environment:
      name: production
      url: https://uaps.so
   only:
      - master
   when: manual

deploy_dev:
   stage: deploy_dev
   allow_failure: false
   tags:
      - docker-dev
   before_script:
      - mkdir -p ~/.ssh
      - chmod 700 ~/.ssh
      - eval $(ssh-agent -s)
      - echo "${CI_SSH_KEY_STAGE}" | tr -d '\r' | ssh-add - >/dev/null
      - echo "${CI_SSH_KNOWN_HOSTS_STAGE}" > ~/.ssh/known_hosts
      - chmod 0644 ~/.ssh/known_hosts
   script:
      - host_cnt=1;
        for host in ${CI_SSH_HOSTS_STAGE}; do
           echo -e "\033[37;1;42m Deploying to server:\033[0m\033[37;1;10m ${host_cnt} \033[0m";
           ssh ${CI_SSH_USER_STAGE}@${host} "mkdir -p /var/www/aps/${CI_COMMIT_REF_SLUG}/static/css/";
           rsync -ral -e ssh --exclude='.*' ${CI_PROJECT_DIR}/dist/ ${CI_SSH_USER_STAGE}@${host}:/var/www/aps/${CI_COMMIT_REF_SLUG}/;
           (( host_cnt++ ));
        done
   environment:
      name: develop/${CI_COMMIT_REF_SLUG}
      url: https://${CI_COMMIT_REF_SLUG}.front.aps.lan
      on_stop: stop_dev

stop_dev:
   stage: deploy_dev
   tags:
      - docker-dev
   before_script:
      - mkdir -p ~/.ssh
      - chmod 700 ~/.ssh
      - eval $(ssh-agent -s)
      - echo "${CI_SSH_KEY_STAGE}" | tr -d '\r' | ssh-add - >/dev/null
      - echo "${CI_SSH_KNOWN_HOSTS_STAGE}" > ~/.ssh/known_hosts
      - chmod 0644 ~/.ssh/known_hosts
   script:
      - host_cnt=1;
        for host in ${CI_SSH_HOSTS_STAGE}; do
           echo -e "\033[37;1;42m Stopping on server:\033[0m\033[37;1;10m ${host_cnt} \033[0m";
           ssh ${CI_SSH_USER_STAGE}@${host} "rm -rf /var/www/aps/${CI_COMMIT_REF_SLUG}";
           (( host_cnt++ ));
        done
   when: manual
   environment:
      name: develop/${CI_COMMIT_REF_SLUG}
      action: stop

deploy_rpm_stage:
   stage: deploy_stage
   allow_failure: false
   tags:
      - docker-dev
   variables:
      CI_CONFIG_SUFFIX: _STAGE
   before_script:
      - mkdir -p ~/.ssh
      - chmod 700 ~/.ssh
      - eval $(ssh-agent -s)
      - echo "${CI_SSH_KEY_STAGE}" | tr -d '\r' | ssh-add - >/dev/null
      - echo "${CI_SSH_KNOWN_HOSTS_STAGE}" > ~/.ssh/known_hosts
      - chmod 0644 ~/.ssh/known_hosts
   script:
      - CI_PACKAGE_FILE=$(cat ${CI_PROJECT_DIR}/deployed.pkg)
      - host_cnt=1;
        for host in ${CI_SSH_HOSTS_STAGE}; do
           echo -e "\033[37;1;42m Deploying to server:\033[0m\033[37;1;10m ${host_cnt} \033[0m";
           ssh ${CI_SSH_USER_STAGE}@${host} "/usr/bin/sudo /usr/bin/yum install -y http://repo.aps.lan/nexus/repository/aps-stage/centos/7/x86_64/${CI_PACKAGE_FILE}";
           (( host_cnt++ ));
        done
#      - ${CI_PROJECT_DIR}/contrib/ci/cloudflare.sh flush-cache
   environment:
      name: staging
      url: https://aps.lan
   only:
      - master
      - develop

deploy_rpm_prod:
   stage: deploy_prod
   allow_failure: false
   tags:
      - docker-prod
   variables:
      CI_CONFIG_SUFFIX: _PROD
   before_script:
      - mkdir -p ~/.ssh
      - chmod 700 ~/.ssh
      - eval $(ssh-agent -s)
      - echo "${CI_SSH_KEY_PROD}" | tr -d '\r' | ssh-add - >/dev/null
      - echo "${CI_SSH_KNOWN_HOSTS_PROD}" > ~/.ssh/known_hosts
      - chmod 0644 ~/.ssh/known_hosts
   script:
      - CI_PACKAGE_FILE=$(cat ${CI_PROJECT_DIR}/deployed.pkg)
      - host_cnt=1;
        for host in ${CI_SSH_HOSTS_PROD}; do
           echo -e "\033[37;1;42m Deploying to server:\033[0m\033[37;1;10m ${host_cnt} \033[0m";
           ssh ${CI_SSH_USER_PROD}@${host} "/usr/bin/sudo /usr/bin/yum install -y http://repo.aps.lan/nexus/repository/aps/centos/7/x86_64/${CI_PACKAGE_FILE}";
           (( host_cnt++ ));
        done
#      - ${CI_PROJECT_DIR}/contrib/ci/cloudflare.sh flush-cache
   environment:
      name: production
      url: https://uaps.so
   only:
      - master
   when: manual

run_simple_test:
  stage: tests
  allow_failure: true
  tags:
    - docker-dev
  before_script:
    - export CI_BRANCH=${CI_COMMIT_REF_NAME}
    - export CI_COMMIT_SHA=${CI_COMMIT_SHA}
    - export CI_BROWSERSTACK_USER=${CI_BROWSERSTACK_USER}
    - export CI_BROWSERSTACK_KEY=${CI_BROWSERSTACK_KEY}
    - export CI_FRONT_USER=${CI_FRONT_USER}
    - export CI_FRONT_PASSWORD=${CI_FRONT_PASSWORD}
    - export CI_BROWSERSTACK_PROJECT="aps-front"
    - export CI_BROWSERSTACK_BUILD=${CI_COMMIT_REF_NAME}
    - export CI_BROWSERSTACK_NAME="${CI_COMMIT_TITLE}"
  script:
    - |
        curl -D- -u "autotest:${CI_AUTOTEST_JIRA_PASSWORD}" \
        -X POST --data "{\"body\": \"$(python -m unittest -v selenium_tests.SimpleTest  2>&1 | tr '\n' '#' | sed 's/#/\\n/g' | sed 's/"/\\"/g')\"}" \
        -H "Content-Type: application/json" \
        "${CI_JIRA_URL}/rest/api/2/issue/${CI_COMMIT_REF_NAME}/comment"
  when: manual

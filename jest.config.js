module.exports = {
    cache: false,
    moduleFileExtensions: ['js', 'vue', 'svg'],
    moduleNameMapper: {
        '^@/(.*)$': '<rootDir>/src/$1'
    },
    transform: {
        '.*\\.(vue)$': 'vue-jest',
        '^.+\\.js$': 'babel-jest',
        '^.+\\.svg$': 'jest-raw-loader'
    },
    transformIgnorePatterns: ['/node_modules/.*'],
    snapshotSerializers: ['jest-serializer-vue']
};

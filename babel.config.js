module.exports = {
    presets: ['@vue/app'],
    env: {
        test: {
            plugins: [
                ['@babel/plugin-proposal-object-rest-spread', { loose: true, useBuiltIns: true }],
                [
                    'module-resolver',
                    {
                        root: ['./src'],
                        alias: {
                            '@': './src'
                        }
                    }
                ]
            ],
            presets: [['@babel/preset-env', { targets: { node: 'current' } }]]
        }
    }
};

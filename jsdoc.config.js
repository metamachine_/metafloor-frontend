const buildVersion = require('./tools/build-version');

module.exports = {
    plugins: ['node_modules/jsdoc-vuejs'],
    source: {
        include: ['src', 'README.md'],
        includePattern: '\\.(vue|js)$'
    },
    opts: {
        template: 'node_modules/ink-docstrap/template',
        destination: './docs/',
        recurse: true,
        sort: false
    },
    templates: {
        systemName: 'frontend',
        linenums: true,
        footer: `<center>build version: ${buildVersion}</center>`,
        copyright: 'frontend',
        theme: 'spacelab'
    }
};
